from django.shortcuts import render
from django.http import JsonResponse
import urllib3
import json

def index(request):
    return render(request, 'story8/spellbook.html')
    
def get_books(request):
    http = urllib3.PoolManager()
    response = http.request(
        "GET", "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('q', ''))
    return JsonResponse(json.loads(response.data.decode('utf8')))
