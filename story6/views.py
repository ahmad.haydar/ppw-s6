from django.shortcuts import render, HttpResponse, redirect, get_object_or_404, reverse
from .models import Peserta, Kegiatan
from django.contrib import messages
from .forms import FormKegiatan, FormPeserta

def index(request):
    context = {
        "kegiatan_items" : Kegiatan.objects.all(),
        "peserta_form" : FormPeserta,
    }
    return render(request,"index.html",context)

def add_peserta(request,id):
    if request.method == "POST":
        kegiatan = Kegiatan.objects.get(id=id)
        nama_peserta_baru = request.POST["nama_peserta"]

        peserta_baru, status_baru = Peserta.objects.get_or_create(nama_peserta=nama_peserta_baru)
        if status_baru:
            peserta_baru.save()
            kegiatan.peserta.add(peserta_baru) # Add peserta baru yang tidak pernah ada.
            messages.add_message(request, messages.SUCCESS, "{} berhasil ditambahkan ke {}".format(nama_peserta_baru,kegiatan.nama_kegiatan))
        else:
            try:
                kegiatan.peserta.get(nama_peserta=nama_peserta_baru) # Jangan add peserta yang sudah di kegiatan.
                messages.add_message(request, messages.WARNING, "{} sudah di kegiatan {}".format(nama_peserta_baru,kegiatan.nama_kegiatan))
            except:
                kegiatan.peserta.add(peserta_baru) # Add peserta baru yang ada di database tapi tidak di kegiatan.
                messages.add_message(request, messages.SUCCESS, "{} berhasil ditambahkan ke {}".format(nama_peserta_baru,kegiatan.nama_kegiatan))
    return redirect(reverse("story6:home"))

def detail_peserta(request,nama):
    nama = nama.replace("-"," ")
    peserta = get_object_or_404(Peserta,nama_peserta=nama)
    context = {
        "peserta" : peserta
    }
    return render(request,"detail.html",context)

def add_kegiatan(request):
    if request.method == "POST":
        kegiatan_form = FormKegiatan(request.POST)
        if kegiatan_form.is_valid():
            kegiatan_object = kegiatan_form.save(commit=False)
            kegiatan_form.save()
        return redirect(reverse("story6:home"))

    context = {
        "kegiatan_form" : FormKegiatan,
        "is_add_kegiatan" : True
    }
    return render(request,"add_kegiatan.html",context)

def delete_peserta(request):
    if request.method == "POST":
        try:
            peserta = Peserta.objects.get(nama_peserta = request.POST["nama"])
            kegiatan = Kegiatan.objects.get(id = request.POST["kegiatan_id"])
        except:
            messages.add_message(request, messages.WARNING, "{} tidak ditemukan {} sebagai partisipan.".format(request.POST["nama"], request.POST["kegiatan_id"]))
            return redirect("story6:home")
        kegiatan.peserta.remove(peserta)
        messages.add_message(request, messages.SUCCESS, "{} berhasil dihapus dari {}.".format(peserta.nama_peserta, kegiatan.nama_kegiatan))
    return redirect("story6:home")