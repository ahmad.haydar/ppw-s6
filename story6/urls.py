from django.urls import path
from . import views

app_name = "story6"

urlpatterns = [
    path("",views.index,name="home"),
    path("add_peserta/<int:id>",views.add_peserta,name="add_peserta"),
    path("profile/<str:nama>",views.detail_peserta,name="detail_peserta"),
    path("add_kegiatan/",views.add_kegiatan, name="add_kegiatan"),
    path("delete_peserta/",views.delete_peserta, name="delete_peserta")
]
