from django import forms
from .models import Kegiatan, Peserta

class FormPeserta(forms.ModelForm):
    nama_peserta = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control w-40",
        "placeholder" : "Peserta Name"
    }), label = "")
    class Meta:
        model = Peserta
        fields = "__all__"

class FormKegiatan(forms.ModelForm):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs = {"class":"form-control w-50 text-center",
        "placeholder" : "Nama Kegiatan"
    }), label = "")
    class Meta:
        model = Kegiatan
        exclude = ["peserta"]
