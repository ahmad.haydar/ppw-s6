var noimage = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/200px-No_image_3x4.svg.png";
var noauthor = "Anonymous"

$(document).ready( function () {
    $( '#search' ).click(function ( event ) {
        event.preventDefault();
        $( '#results' ).innerHTML = "";
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + $( '#search-query' ).val(),
            success: addResult
        });
    })
})

function addResult( result ) {
    let json = result.items;
    let inner = "";
    for (let i = 0; i < json.length; i++) {
        let data = json[i].volumeInfo;
        inner +=
`
<div class="card shadow-lg text-dark w-75 mx-auto my-3">
      <h3 class="card-header bg-main1 text-center text-left-lg">${data.title}(by ${(data.authors === undefined ? noauthor : data.authors)})</h3>
      <div class="card-body bg-main2">
        <div class="row">
          <div class="col-lg-3 col-12 d-flex align-items-center justify-content-center">
            <img class="mr-3 book-cover" src="${(data.imageLinks === undefined ? noimage : data.imageLinks.thumbnail)}">
          </div>
          <div class="col-lg-9 col-12 book-info">
            <h5 class="card-title">Published on ${data.publishedDate}</h5>
            <p class="card-text text-justify" style="overflow: hidden;text-overflow: ellipsis; max-height: 10rem;">${data.description ? data.description : "No description available"}</p>
          </div>
        </div>
      </div>
    </div>
`
    }
    
    $( '#results ').html(inner);
}