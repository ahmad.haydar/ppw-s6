from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login , logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm, AuthenticationForm

# Login System
def register(request):
    if request.user.is_authenticated:
        return redirect("story7:index")
    context = {
        "registering":RegistrationForm,
    }

    if request.method == "POST":
        registering = RegistrationForm(request.POST)
        if registering.is_valid():
            registering.save()
            new_user_account = authenticate(
                username = registering.cleaned_data['username'],
                password = registering.cleaned_data['password1']
            )
            login(request, new_user_account)

            return redirect("story8:index")
        elif registering.errors:
            context["error"] = "Wrong input!"
    return render(request, 'story9/register.html', context)

def log_in(request):
    context = {"login": LoginForm}
    if request.user.is_authenticated:
        return redirect("story8:index")

    if request.method == "POST":
        logging_in = LoginForm(data = request.POST)
        if logging_in.is_valid():
            user_account = logging_in.get_user()
            login(request, user_account)
            return redirect("story8:index")
        elif logging_in.errors:
            context["error"] = "Wrong Username or Password"
    return render(request, 'story9/login.html', context)

def log_out(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("story8:index")
