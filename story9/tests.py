from django.test import TestCase, Client

class loginFunctionTests(TestCase):
    def test_url_register(self):
        resp = Client().get('/story9/register/')
        self.assertEquals(200, resp.status_code)
    
    def test_url_login(self):
        resp = Client().get('/story9/login/')
        self.assertEquals(200, resp.status_code)

    def test_url_logout(self):
        resp = Client().get('/story9/logout/')
        self.assertEquals(302, resp.status_code)
    
    def test_register_correct(self):
        resp = Client().post(f'/story9/register/', {
            "username": "fefefec",
            "password1": "Haydar1010",
            "password2": "Haydar1010",
        })
        self.assertEqual(resp.status_code, 302)
    
    def test_register_correcxxt(self):
        resp = Client().post(f'/story9/register/', {
            "username": "fefefec",
            "password1": "Haydar1010",
            "password2": "m",
        })
        self.assertEqual(resp.status_code, 200)
    
    def test_login_true(self):
        resp = Client().post(f'/story9/register/', {
            "username": "fefefec",
            "password1": "Haydar1010",
            "password2": "Haydar1010",
        })
        resp = Client().post(f'/story9/login/', {
            "username": "fefefec",
            "password": "Haydar1010",
        })
        self.assertEqual(resp.status_code, 302)
    
    def test_login_false(self):
        resp = Client().post(f'/story9/register/', {
            "username": "fefefec",
            "password1": "Haydar1010",
            "password2": "Haydar1010",
        })
        resp = Client().post(f'/story9/login/', {
            "username": "fefefec",
            "password": "Haydarsss1010",
        })
        self.assertEqual(resp.status_code, 200)