from django.test import TestCase, Client
from django.urls import reverse
import json
# Create your tests here.

class story8Test(TestCase):
    def test_GET_index(self):
        response = self.client.get(reverse('story8:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"story8/spellbook.html")

    def test_GET_API_page(self):
        response = self.client.get(reverse('story8:get_books') + '?q=Imagined%20Communities')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Imagined Communities', response.content.decode('utf-8'))
