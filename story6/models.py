from django.db import models
from django.shortcuts import reverse

class Peserta(models.Model):
    nama_peserta = models.TextField(max_length=50, unique=True)

    def __str__(self):
        return self.nama_peserta
    
    def get_absolute_url(self):
        nama_url = self.nama_peserta.replace(" ","-")
        return reverse("story6:detail_peserta",args=[nama_url])

class Kegiatan(models.Model):
    nama_kegiatan = models.TextField(max_length=50,unique=True)
    peserta = models.ManyToManyField(Peserta,blank=True)

    def __str__(self):
        return self.nama_kegiatan