from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Peserta, Kegiatan
from .views import index, add_peserta, detail_peserta

# Create your tests here.
class TestUrlsViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("story6:home")
        self.kegiatan = Kegiatan.objects.create(nama_kegiatan = "kegiatan satu")
        self.add_peserta_url = reverse("story6:add_peserta",args=[self.kegiatan.id])
        self.ada_peserta = Peserta.objects.create(nama_peserta="ada peserta")
        self.detail_peserta_url = reverse("story6:detail_peserta",args=["ada peserta"])
        self.add_kegiatan_url = reverse("story6:add_kegiatan")
        self.peserta_to_be_gameended = Peserta.objects.create(nama_peserta="game end me")
        self.delete_peserta_url = reverse("story6:delete_peserta")
    
    def test_index_url_GET(self): #Siapin index
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"index.html")
    
    def test_detail_peserta_url_GET(self): #Siapin detail peserta
        response = self.client.get(self.detail_peserta_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"detail.html")
    
    def test_add_kegiatan_url_GET(self): #Siapin Kegiatan add
        response = self.client.get(self.add_kegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"add_kegiatan.html")
    
    def test_add_kegiatan_redirect_add_POST(self): #Siapin nambah kegiatan
        response = self.client.post(self.add_kegiatan_url, {
            "nama" : "nama kegiatan"
        })
        self.assertEquals(response.status_code,302)
    
    def test_delete_peserta_found_POST(self):
        #Add peserta
        self.kegiatan.peserta.add(self.peserta_to_be_gameended)
        old_length = len(self.kegiatan.peserta.all())

        #Delete
        response = self.client.post(self.delete_peserta_url, {
            "nama" : "game end me",
            "kegiatan_id" : self.kegiatan.id
        })
        self.assertEquals(len(self.kegiatan.peserta.all()), old_length-1) #Jika kedelete kekurang 1
        self.assertEquals(response.status_code, 302)

    def test_delete_peserta_POST_peserta_ded(self):
        response = self.client.post(self.delete_peserta_url, {
            "nama" : "not_found",
            "kegiatan_id" : self.kegiatan.id
        }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "tidak ditemukan")
    
    def test_add_peserta_POST_name_on_database(self):
        response = self.client.post(self.add_peserta_url, {
            "nama_peserta" : "ada peserta"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama_peserta,"ada peserta")
    
    def test_add_peserta_POST_adds_nama(self):
        response = self.client.post(self.add_peserta_url, {
            "nama_peserta" : "test_nama"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama_peserta,"test_nama")

class TestModels(TestCase):
    def setUp(self):
        self.peserta_test = Peserta.objects.create(
            nama_peserta = "peserta new"
        )
        self.kegiatan_test = Kegiatan.objects.create(
            nama_kegiatan = "kegiatan new"
        )
        self.kegiatan_test.peserta.add(self.peserta_test)

    def test_nama_peserta(self):
        self.assertEquals(str(self.peserta_test),"peserta new")

    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatan_test),"kegiatan new")

    def test_absolute_url_peserta(self):
        self.assertEquals(self.peserta_test.get_absolute_url(),"/profile/peserta-new")

    


